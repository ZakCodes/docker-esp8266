# Baser l'image sur openSUSE Tumbleweed
FROM opensuse/tumbleweed

# Installer les dépendances
RUN zypper refresh && zypper install -y git python3 curl tar wget which

# Copier et exécuter setup.sh
COPY setup.sh ~/setup.sh
RUN ["/bin/bash", "~/setup.sh"]

# Désinstaller les dépendances qui n'étaient
# nécessaires que pour l'exécution de setup.sh
RUN zypper remove -y curl tar wget which
