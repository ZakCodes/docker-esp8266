#!/bin/bash

# Script pour configurer une image docker basée sur opensuse/tumbleweed pour
# compiler des sketch Arduino pour le esp8266

# Bibliothèques externes nécessaires pour le projet
bibliotheques=(
)

# URL pour télécharger la board
boardUrl=https://arduino.esp8266.com/stable/package_esp8266com_index.json

# Quitter le script à la première erreur survenue
set -e

# Télécharger et installer arduino-cli
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=/usr/bin sh

# Installer les board avec un microcontrôleur esp8266
arduino-cli --additional-urls $boardUrl core update-index
arduino-cli --additional-urls $boardUrl core install esp8266:esp8266

# Télécharger toutes les bibliothèques
arduino-cli lib update-index
for bibliotheque in "${bibliotheques[@]}"; do
    arduino-cli lib install "$bibliotheque"
done
