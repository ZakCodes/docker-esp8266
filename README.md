# Docker pour [arduino-cli] et le [ESP8266]
Ce docker contient l'outil [arduino-cli](https://github.com/arduino/arduino-cli) permettant de compiler un projet à partir de la ligne de commande et la board esp8266.

Il est à noter que cette image est 4x plus petite que l'image docker [officielle d'arduino-cli](https://hub.docker.com/r/arduino/arduino-cli/tags). De plus, l'image officielle ne semble pas être maintenue du tout.

## Utilisation
Vous pouvez utiliser ce projet de deux différentes manières. Vous pouvez le cloner et le modifier pour y ajouter des dépendances ou vous pouvez l'utiliser tel quel. Si vous n'avez pas besoin d'y ajouter de dépendances, sautez tout de suite [à cette section](#utiliser-limage-dans-gitlab-ci).

### Modifier les dépendances
La première étape est de modifier le fichier [`setup.sh`] pour y ajouter les bibliothèques Arduino requises pour votre projet. Elles doivent être ajoutées à la liste dans la variable `bibliotheques` tout au sommet du fichier.

Les noms des dépendances à ajouter sont les mêmes que ceux que vous pouvez voir dans le Gestionnaire de Libraries du Arduino IDE. **Ce n'est pas forcément la même chose que le nom du `.h`.**

Voici un exemple de comment ajouter les bibliothèques `DHT sensor library for ESPx` et `ArduinoJson`.
```diff
bibliotheques=(
+    "DHT sensor library for ESPx"
+    "ArduinoJson"
)
```

### Construire l'image
Pour construire votre image, vous pouvez utiliser le script [`create.sh`].

Avant de l'exécuter vous pouvez modifier la variable `IMAGE` dans ce script pour changer le nom de l'image qui sera créée par ce script. Le nom par défaut est `zakcodes/arduino-esp8266:latest`, `zakcodes` est votre [Docker ID], `arduino-esp8266` est le nom de l'image et `latest` est le nom ou le numéro de version de l'image. Vous pouvez omettre le [Docker ID], et le `/`, si vous ne comptez pas publier l'image sur le [Docker Hub]. Si vous omettez la version de l'image, et le `:`, `latest` sera considéré comme la version de l'image.

Le script va créer l'image que vous pourrez ensuite tester avec la commande `docker run -it <nom de votre image>`. Ceci exécute un terminal interactif où vous pouvez cloner votre projet et le compiler pour vous assurer que tout fonctionne.

### Publier l'image sur le [Docker Hub]
Afin d'utiliser l'image dans [GitLab CI], vous pouvez la publier sur le [Docker Hub] qui est un service utiliser pour partager des images.  
Il est à noter que les images qui y sont téléversées sont publiques, donc ne mettez rien de secret dans vos images. Si vous avez besoin de cacher quelque chose, utiliser [des variables protégées de GitLab CI](https://docs.gitlab.com/ee/ci/variables/).

Avant de pouvoir publier l'image, vous devrez vous [créer un compte sur le Docker Hub](https://hub.docker.com/signup/). Une fois fait, vous devrez vous connecter à votre compte à partir de la ligne de commande comme ceci:
```sh
docker login
```
Vous devrez alors entre votre [Docker ID] et votre mot de passe.

```sh
docker push <Docker ID>/<nom de l'image>:<version>
```

### Utiliser l'image dans [GitLab CI]
Pour l'utiliser, vous avez seulement besoin de créer un fichier `.gitlab-ci` à la racine de votre git repository. Voici un exemple de script pour ce fichier:
```sh
build:
  image: zakcodes/arduino-cli:latest
  stage: build
  script:
    - arduino-cli compile -v --warnings all --fqbn esp8266:esp8266:nodemcuv2 Sketch.ino
```

Il vous suffit ensuite de changer `nodemcuv2` pour la variante de votre board. Le nom de chaque version correspond à la liste des noms de dossiers [ici](https://github.com/esp8266/Arduino/tree/master/variants). Vous pouvez aussi changer l'image pour celle que vous avez publiées sur le [Docker Hub].

Vous devrez également modifier le nom `Sketch.ino` pour le chemin de votre sketch Arduino.

## License
Tout le contenu de projet est sous la licence MIT.

[ESP8266]: https://github.com/esp8266/Arduino
[arduino-cli]: https://github.com/arduino/arduino-cli
[arduino-cli-releases]: https://github.com/arduino/arduino-cli/releases
[Docker Hub]: https://hub.docker.com/
[Docker ID]: https://docs.docker.com/docker-id/
[GitLab CI]: https://docs.gitlab.com/ee/ci/
[`setup.sh`]: ./setup.sh
[`create.sh`]: ./create.sh
