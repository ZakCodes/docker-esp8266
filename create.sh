#!/bin/bash

# Script pour créer une image docker à partir du Dockerfile

# Nom du docker à créer
IMAGE=zakcodes/arduino-esp8266:latest

# Obtenir la version la plus récente de l'image sur laquelle est basée celle à créer
docker pull opensuse/tumbleweed

# Construire le docker
docker build -t $IMAGE .

# Publier l'image sur le Docker Hub
# docker login
# docker push $IMAGE
